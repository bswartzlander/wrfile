#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include <sys/stat.h>

#define BUF_SZ 4096

static void
usage(void)
{
	fputs("Usage: wrfile [ -m <mode> ] <filename>\n", stderr);
	exit(2);
}

int
main(int argc, char ** argv)
{
	char * filename;
	mode_t mode = 0644;
	if (4 == argc && 0 == strcmp("-m", argv[1])) {
		char * end;
		errno = 0;
		mode = strtoul(argv[2], &end, 8);
		printf("end=%s errno=%d\n", end, errno);
		if ('\0' != *end || 0 != errno) {
			usage();
		}
		filename = argv[3];
	} else if (2 == argc) {
		filename = argv[1];
	} else {
		usage();
	}

	umask(0);
	int fd = open(filename, O_WRONLY | O_TRUNC | O_CREAT, mode);
	if (0 > fd) {
		perror("Failed to create file");
		exit(1);
	}

	while (1) {
		char buffer[BUF_SZ];
		ssize_t n = read(STDIN_FILENO, buffer, BUF_SZ);
		if (0 > n) {
			perror("Failed to read stdin");
			exit(1);
		} else if (0 == n) {
			break;
		}
		n = write(fd, buffer, n);
		if (0 > n) {
			perror("Failed to write to file");
			exit(1);
		}
	}

	close(fd);

	return 0;
}
